module.exports = {
  'env': {
    'browser': true,
    'es6': true,
    'jest/globals': true,
  },
  'extends': [
    'airbnb',
    'plugin:@typescript-eslint/recommended',
    'prettier',
    'prettier/@typescript-eslint',
    'prettier/react',
  ],
  'globals': {
    'Atomics': 'readonly',
    'SharedArrayBuffer': 'readonly',
  },
  'parser': '@typescript-eslint/parser',
  'parserOptions': {
    'ecmaFeatures': {
      'jsx': true,
    },
    'ecmaVersion': 2018,
    'sourceType': 'module',
  },
  'plugins': [
    'import',
    'jest',
    'prettier',
    'react',
    'react-hooks',
    '@typescript-eslint',
  ],
  'rules': {
    'object-shorthand': [
      'error',
      'never',
    ],
    'no-underscore-dangle': [
      'error',
      {
        'allow': [
          '__REDUX_DEVTOOLS_EXTENSION_COMPOSE__',
        ],
      },
    ],
    'import/order': [
      'error',
      {
        'groups': [
          'builtin',
          'external',
          'internal',
          'parent',
          'sibling',
          'index',
        ],
        'newlines-between': 'always',
      },
    ],
    'prettier/prettier': 'error',
    'react/jsx-filename-extension': [
      2,
      {
        'extensions': [
          '.jsx',
          '.tsx',
        ],
      },
    ],
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'error',
    '@typescript-eslint/no-inferrable-types': [
      false,
    ],
    '@typescript-eslint/no-unused-vars': [
      'error',
    ],
  },
  'settings': {
    'import/ignore': [
      '\\*.svg',
    ],
    'import/resolver': {
      'typescript': {},
    },
    'typescript': {
      'directory': './',
    },
  },
};