# ESLint Config Ixirsii

## About

Ixirsii's React/Typescript ESLint config

## Usage

Add a dev dependency on the package
```
npm i -D eslint-config-ixirsii
```

Extend the config in your .eslintrc.json

```JSON
{
  "extends": ["eslint-config-ixirsii"]
}
```